## KiCAD Cheatsheets 
  
These sub folders contain the original Anthony Gautier KiCAD v5 cheatsheet and an updated KiCAD v6, and v7 cheatsheet PDF,along with a Scribus and image files if you want to make your own modifications.
  
A v8 cheatsheet might be created later in 2024?

License: CC-BY-SA 4.0
 
